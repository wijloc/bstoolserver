const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

app.get('/items', (req, res) => {
	const MongoClient = require('mongodb').MongoClient

	MongoClient.connect('mongodb+srv://readOnly:9nMEnqc9NDBAomB1@cluster0-rgnxr.gcp.mongodb.net/test?retryWrites=true&w=majority', (err, client) => {
	  if (err) return console.log(err)
	  db = client.db('bloodstone')
	  db.collection('items').find(
	    { $text: { $search: req.query.text} },
		{
			projection: {score:{$meta:"textScore"}},
			score: { $meta: "textScore" } 
		}
      ).sort( { score: { $meta: "textScore" }, name: 1, operation: -1 } ).toArray(function(err, results) {
		  if (err)
			  console.log(err);
		  res.send(results);		  
	  })
	  client.close();
	});
})

app.listen(process.env.PORT || 8081)